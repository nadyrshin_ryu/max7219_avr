//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "delay\delay.h"
#include "main.h"
#include "scroll_demo.h"
#include "delay\delay.h"
#include "max7219\ledmatrix.h"
#include "max7219\fonts\font.h"


// ����� ��� ������� �� ����������
uint8_t ScrollBuff[140];

void main( void )
{
  // �������������� ������������ �������
  ledmatrix_init();
  
  // ������ ����� ��������� (���� ���� �����������)
  ledmatrix_testmatrix(1);
  delay_ms(500);
  ledmatrix_testmatrix(0);

  while (1)
  {
    // ��������� ����� ��� ������� �� ������� � �������� �������
    // ��� ���������������� ������ ����� ����� ������������ ������� font_printf
    uint16_t ScrollLines = font_DrawString("����������� � ���������", ScrollBuff, 140);
    // �������� ����� �� ������� ������ ������
    demo_ScrollBuff(ScrollBuff, ScrollLines, 1);
    // �������� ����� �� ������� ����� �������
    demo_ScrollBuff(ScrollBuff, ScrollLines, 0);
  }
}
